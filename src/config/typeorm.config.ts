import { TypeOrmModuleOptions } from '@nestjs/typeorm';

import { Task } from './../tasks/task.entity';
import { User } from 'src/auth/user.entity';

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'openpg',
  password: 'openpgpwd',
  database: 'nest',
  entities: [Task, User],
  synchronize: true,
};
